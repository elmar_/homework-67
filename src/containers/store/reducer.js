const initState = {
    code: '',
    codeLength: 0,
    codeAccess: 7091,
    codeClass: 'inherit',
    codeShow: '',
    calcValue: '',
    over: 0
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case 'code':
            if (state.codeClass !== 'inherit') {
                return {
                    ...state,
                    code: action.value,
                    codeLength: 1,
                    codeShow: '*'.repeat(1),
                    codeClass: 'inherit'
                };
            } else return {
                ...state,
                code: state.code + action.value,
                codeLength: state.code.length + 1,
                codeShow: '*'.repeat(state.codeLength + 1)
            };
        case 'E':
            console.log(action.type);
            if (state.codeAccess === parseInt(state.code)) {
                return {...state, codeShow: 'Access Granted', codeClass: 'green'};
            } else return {...state, codeShow: 'Access Denied', codeClass: 'red'};
        case 'del':
            if (state.codeLength <= 0) {
                return state;
            } else return {
                ...state,
                code: state.code.substring(0, state.codeLength - 1),
                codeLength: state.codeLength - 1,
                codeShow: '*'.repeat(state.codeLength - 1),
                codeClass: 'inherit'
            };

            // CALC

        case 'calc':
            return {...state, calcValue: state.calcValue + action.value, over: -1};
        case 'number':
            return {...state, calcValue: state.calcValue + action.value, over: state.over + 1};
        case 'count':
            return {...state, calcValue: action.value};
        case 'deleteNum':
            if (state.calcValue.length <= 0) {
                return state;
            } else return {
                ...state,
                calcValue: state.calcValue.substring(0, state.calcValue.length - 1),
                over: state.over + 1
            };

        default:
            return state;
    }
};

export default reducer;