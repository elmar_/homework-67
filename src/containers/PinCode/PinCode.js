import React from 'react';
import './PinCode.css';
import {useDispatch, useSelector} from "react-redux";

const PIN = ['7', '8', '9', '4', '5', '6', '1', '2', '3', 'del', '0', 'E'];
const NUMBERS = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '+', '-', '/', '*', 'del', '='];

const PinCode = () => {
    const dispatch = useDispatch();
    const code = useSelector(state => state);

    const dispatchingCode = value => {
        if (isNaN(parseInt(value))) {
            dispatch({type: value});
        } else {
            if (code.codeClass === 'inherit' && code.codeLength >= 4) {
                alert('Максимум длина кода 4');
            } else dispatch({type: 'code', value: value});
        }
    };

    const dispatchingCalc = value => {
        if (value === '=') {
            const answer = eval(code.calcValue);
            dispatch({type: 'count', value: answer.toString()});
        } else if (value === 'del') {
            dispatch({type: 'deleteNum'});
        } else if (isNaN(parseInt(value))) {
            if (code.over === -1) alert('Нельзя вводить две операции подрят');
            else dispatch({type: 'calc', value: value});
        } else dispatch({type: 'number', value: value});
    };

    console.log(code)

    return (
        <div className="PinCode">
            <div className="box">
                <p>Code is {code.codeAccess}</p>
                <input
                    type="text"
                    disabled
                    className='inp'
                    value={code.codeShow}
                    style={{color: code.codeClass}}
                />
                <div>
                    {PIN.map(num => <span key={num} onClick={() => dispatchingCode(num)}>{num}</span>)}
                </div>
            </div>

            <div className="box">
                <p>Calc</p>
                <input
                    type="text"
                    disabled
                    className='inp'
                    value={code.calcValue}
                />
                <div>
                    {NUMBERS.map(num => <span key={num} onClick={() => dispatchingCalc(num)}>{num}</span>)}
                </div>
            </div>
        </div>
    );
};

export default PinCode;